import gym
import time
import os, glob, pickle, gym, assistive_gym, argparse
import numpy as np
import json
from shutil import copyfile
import os
from PIL import Image
from numpngw import write_apng

camera_configs = {
	"BedBathing": {
		"camera_eye": [-0.7, -0.75, 1.5],
		"camera_target": [0.0, 0, 0.75],
		"camera_width": 1920 // 4,
		"fov": 60,
		"camera_height": 1080 // 4
	}
}
# save_fig = True
save_fig = False


data_dir = "/Users/jerry/Dropbox/Projects/AssistRobotics/assistive-gym-fem/assistive_gym/envs/assets/poses_v1/"
# data_dir = "/Volumes/Blade/Alienware Backup/Assist/vr/assistive-gym-backup/poses/"

env = gym.make("BedBathingJaco-v1")

for data_path in sorted(os.listdir(data_dir)):
	if not ".json" in data_path:
		continue

	pose_data = []

	with open(os.path.join(data_dir, data_path), "r") as f:
		pose_data = json.load(f)

	# import pdb; pdb.set_trace()
	if save_fig:
		# data_name = data_path.split("/")[-1].replace(".json", "")
		data_name = "test_pose"
		os.makedirs(f"new_images/{data_name}", exist_ok=True)
		env.setup_camera(**camera_configs["BedBathing"])


	## TODO: angle of the bed
	## TODO: position of the robot


	env.render()
	env.reset()

	idx = 0
	frames = []

	JOINT_MAPPING = {
	    # 0: 0, # waist
	    # 1: 1, # waist
	    # 2: 2, # waist
	    # 3: 3, # chest
	    # 4: 4, # chest
	    # 5: 5, # chest
	    0: 3, # waist
	    1: 4, # waist
	    2: 5, # waist
	    6: 6, # upper chest
	    7: 12, # left shoulder
	    8: 13, # left shoulder
	    9: 14, # left shoulder
	    10: 15, # elbow
	    11: 16, # hand
	    12: 17, # hand
	    13: 18, # hand
	    14: -1,
	    15: -1,
	    16: -1,
	    17: 22, # right arm
	    18: 23, # right arm
	    19: 24, # right arm
	    20: 25, # right arm
	    21: 26, # right arm
	    22: 27, # right arm
	    23: 28,  # right arm
	    24: -1,  # right arm
	    25: 30, # head joint
	    26: 31, # head joint
	    27: 32, # head joint
	}

	while idx <= 199:
		converted_data = {}
		for j, data in pose_data[idx].items():
			if int(j) in JOINT_MAPPING:
				converted_data[JOINT_MAPPING[int(j)]] = data
		env.set_human_joints(converted_data)
		# for j, data in pose_data[idx].items():
		for j, data in converted_data.items():
			print(f"Joint {j} state {data}")
		# env.set_human_joints({15: (-1.5, 0)})
		# env.set_human_joints({4: (0.9, 0)})
		# env.set_human_joints({
		# 	0: (0.054432942776627004, 5.077387759360136e-06),
		# 	1: (0.0901584577948794, 3.3508111559493216e-06),
		# 	2: (0.03830926347787594, -5.983809090170857e-05),
		# 	# 4: (0.9, 0)
		# 	12: (0.3490658503988659, 0.0),
		# 	13: (-0.3490658503988659, 0.0),
		# 	15:(-0.7853981633974483, 0.0),
		# })
		env.render()

		# import pdb; pdb.set_trace()
		# env.step(np.zeros(env.action_space.shape))
		print("render", idx)
		idx += 1
		# time.sleep(0.2)
		time.sleep(1)

		if save_fig:
			img, _ = env.get_camera_image_depth()
			frames.append(img)
			img = Image.fromarray(img)
			img.save(f"new_images/{data_name}/fem_{idx:03d}.png")
	if save_fig:
		write_apng(f"new_images/{data_name}/fem_video.png", frames, delay=100)
