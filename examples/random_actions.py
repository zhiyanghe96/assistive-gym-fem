import gym, assistive_gym

# env = gym.make('BedBathingFemJacoHuman-v0217_h3-v1')
env = gym.make('BedBathingFemJaco210909Ver0_03-v1')
env.render()
observation = env.reset()

while True:
    env.render()
    # action = {
    #     "human": env.action_space_human.sample(), # Get a random action
    #     "robot": env.action_space_robot.sample()
    # }
    action = env.action_space_robot.sample()
    observation, reward, done, info = env.step(action)
    # import pdb; pdb.set_trace()