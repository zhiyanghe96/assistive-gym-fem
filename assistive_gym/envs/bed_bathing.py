import time
import json
import torch
import numpy as np
import pybullet as p
import os, configparser
from gym import spaces
from dotmap import DotMap
from .env import AssistiveEnv
from .agents import furniture
from .agents.furniture import Furniture
from .pose_replay import PosePlayer

class BedBathingEnv(AssistiveEnv):
    def __init__(self,
                 robot,
                 human,
                 collab_version="v0217_0",
                 fix_base=False,
                 impairment="random",
                 latent_args=None,
                 return_robot_only=False,
                 offset_version=None,
                 no_furniture_collision=False,
                 motion_pca_file=None,
                 replay_pose_file=None,
                 init_success_threshold=0.03
                ):

        # Motion PCA
        self.motion_pca_file = motion_pca_file
        self.motion_pca_components = None
        self.motion_pca_keys = None
        self.motion_pca_scale = None
        if self.motion_pca_file is not None:
            with open(self.motion_pca_file, "r") as f:
                pca_data = json.load(f)
                self.motion_pca_components = np.array(pca_data["components"])
                self.motion_pca_keys = pca_data["keys"]
                self.motion_pca_scale = np.array(pca_data["scale"])


        super(BedBathingEnv, self).__init__(robot=robot, human=human, task='bed_bathing', obs_robot_len=(17 + len(robot.controllable_joint_indices) - (len(robot.wheel_joint_indices) if robot.mobile else 0)), obs_human_len=(18 + len(human.controllable_joint_indices)), return_robot_only=return_robot_only)
        self.env_configp = configparser.ConfigParser()
        self.env_configp.read(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'bed_config.ini'))
        self.c_ver = collab_version
        self._fix_base = fix_base
        self._robot_offset = None
        self._impairment = impairment

        # Latent embedding
        self.latent_args = latent_args
        self.latent_current_human = None
        self.return_robot_only = return_robot_only
        self.no_furniture_collision = no_furniture_collision
        self.human_policies = {}
        self._last_human_action = None
        self._last_human_obs = None

        # Important: resets action space, needs to preceed latent_args
        if self.motion_pca_file is not None:
            pca_len = len(self.motion_pca_components) if self.human.controllable else 0
            self.action_space_human = spaces.Box(low=np.array([-1.0]*pca_len, dtype=np.float32), high=np.array([1.0]*pca_len, dtype=np.float32), dtype=np.float32)
            num_controllable_joints = len(self.human.controllable_joint_indices)
            self.action_space_human_pose = spaces.Box(low=np.array([-1.0]*num_controllable_joints, dtype=np.float32), high=np.array([1.0]*num_controllable_joints, dtype=np.float32), dtype=np.float32)
            if return_robot_only:
                self.action_space = spaces.Box(low=np.array([-1.0]*(self.action_robot_len), dtype=np.float32), high=np.array([1.0]*(self.action_robot_len), dtype=np.float32), dtype=np.float32)
            else:
                self.action_space = spaces.Box(low=np.array([-1.0]*(self.action_robot_len+pca_len), dtype=np.float32), high=np.array([1.0]*(self.action_robot_len+pca_len), dtype=np.float32), dtype=np.float32)

        if latent_args is not None:
            if latent_args["load_pytorch"]:
                from . import bed_bathing_latent
                from adacare.assist.utils import get_pytorch_policy
                # Load pre-saved human pytorch policies
                if hasattr(bed_bathing_latent, latent_args["human_paths"]):
                    human_paths =  getattr(bed_bathing_latent, latent_args["human_paths"])
                for human_key in self.latent_args["human_keys"]:
                    path = os.path.join(os.path.dirname(human_paths[human_key]), "human_policy_v1.pth")
                    policy_args = DotMap({"policy_id": "human", "policy_type": "PolicyNetwork"})
                    obs_human_len_=(18 + len(human.controllable_joint_indices))
                    self.human_policies[human_key] = get_pytorch_policy(env_name=None, env=None, path=path, policy_args=policy_args, obs_dim=obs_human_len_, acs_dim=self.action_space_human.shape[0]).float()

        # Initial threshold (2022.01.31 DEPRECATED)
        self._init_success_threshold = init_success_threshold


        # Replay Pose file
        self._replay_pose_player = None
        if replay_pose_file is not None:
            self._replay_pose_player = PosePlayer(self, replay_pose_file, self.task)


    def update_action_space(self):
        action_len = 0
        for a in self.agents:
            if a == self.human and self.motion_pca_file is not None:
                action_len += len(self.motion_pca_components)
            else:
                action_len += len(a.controllable_joint_indices)

        self.action_space.__init__(low=-np.ones(action_len, dtype=np.float32), high=np.ones(action_len, dtype=np.float32), dtype=np.float32)


    def env_config(self, tag, section=None):
        return float(self.env_configp[self.task if section is None else section][tag])

    def set_human_joints(self, joint_data):
        indices, angles, velocities = [], [], []
        for idx, data in joint_data.items():
            indices.append(int(idx))
            angles.append(data[0])
            velocities.append(data[1])
        self.human.set_joint_angles(indices, angles, use_limits=False, velocities=velocities)
        self.set_joint_mode = True
        # print(f"Human base pos orientation {p.getBasePositionAndOrientation(self.human.body)}")

    def step(self, action):
        human_action, robot_action = 0, action
        if self.human.controllable:
            if (self._replay_pose_player is not None) or \
                (self.latent_args is not None and self.latent_args["load_pytorch"]):
                # Compute human actions within env
                human_action = self._last_human_action
            else:
                # Receive human actions
                if self.latent_current_human is not None:
                    human_action = action[self.latent_current_human]
                else:
                    human_action = action['human']

            if self.return_robot_only:
                robot_action = action
            else:
                robot_action = action['robot']

            if  self._replay_pose_player is not None:
                human_action_ = human_action
            elif self.motion_pca_components is not None:
                human_action = np.clip(human_action, a_min=self.action_space_human.low, a_max=self.action_space_human.high)
                human_action_ = self.motion_pca_components.T.dot(human_action[:, None] * self.motion_pca_scale)[:, 0]
            else:
                human_action_ = human_action
            action = np.concatenate([robot_action, human_action_])

        # self.take_step implicitly enforces joint action limits
        self.take_step(action, replay_pose=self._replay_pose_player is not None)

        obs = self._get_obs()

        # Robot-only environment, compute and cache human actions
        if self._replay_pose_player is not None:
            self._last_human_action = self._replay_pose_player.get_next_action(self.iteration)
        elif self.latent_args is not None:
            robot_obs = obs["robot"]
            human_obs = obs["human"]
            del obs["robot"]
            del obs["human"]
            obs[self.latent_current_human] = human_obs
            obs["robot"] = robot_obs

            # Store internal human actions
            if self.latent_args["load_pytorch"]:
                self._last_human_action, _ = self.human_policies[self.latent_current_human].compute_action(torch.tensor([human_obs]).float(), explore=True)
                self._last_human_action = self._last_human_action.numpy()[0]

        # Get human preferences
        end_effector_velocity = np.linalg.norm(self.robot.get_velocity(self.robot.left_end_effector))
        preferences_score = self.human_preferences(end_effector_velocity=end_effector_velocity, total_force_on_human=self.total_force_on_human, tool_force_at_target=self.tool_force_on_human)

        MAX_DISTANCE = 5.
        closest_dists = self.tool.get_closest_points(self.human, distance=5.0)[-1]
        reward_distance = -min(closest_dists) if len(closest_dists) > 0 else -MAX_DISTANCE
        reward_action = -np.linalg.norm(action) # Penalize actions
        reward_action_h = -np.linalg.norm(human_action)
        reward_action_r = -np.linalg.norm(robot_action)
        reward_new_contact_points = self.new_contact_points # Reward new contact points on a person

        penalty_h = self.env_config("human_action_penalty", self.c_ver)
        penalty_r = self.env_config("robot_action_penalty", self.c_ver)

        reward_collab_h = reward_action_h * penalty_h
        reward_collab_r = reward_action_r * penalty_r

        reward_base = self.config('distance_weight')*reward_distance + self.config('wiping_reward_weight')*reward_new_contact_points + preferences_score
        reward = reward_base + self.config('action_weight')*reward_action


        if self.gui and self.tool_force_on_human > 0:
            print('Task success:', self.task_success, 'Force at tool on human:', self.tool_force_on_human, reward_new_contact_points)

        info = {
            'total_force_on_human': self.total_force_on_human,
            'task_success': min(self.task_success / (self.total_target_count*self.config('task_success_threshold')), 1),
            'action_robot_len': self.action_robot_len,
            'action_human_len': self.action_human_len,
            'obs_robot_len': self.obs_robot_len,
            'obs_human_len': self.obs_human_len,
            'task_reward': self.config('wiping_reward_weight')*reward_new_contact_points + self.config('distance_weight')*reward_distance,
            "preference_reward": preferences_score,
            "reward_action_robot": reward_action_r,
            "reward_action_human": reward_action_h,
            'action_weight': self.config('action_weight'),
            'human_action_weight':  self.env_config("human_action_penalty", self.c_ver),
            'robot_action_weight':  self.env_config("robot_action_penalty", self.c_ver),
            'latent_human': self.latent_current_human,
            'last_human_action': self._last_human_action,
            'last_human_obs': self._last_human_obs
        }
        done = self.iteration >= 200

        if not self.human.controllable:
            reward += reward_collab_h + reward_collab_r
            return obs, reward, done, info
        else:
            # Co-optimization with both human and robot controllable
            # print(f"Human collab reward {reward_collab_h}")
            reward_h = reward_base + reward_collab_h
            reward_r = reward_base + reward_collab_r

            ## Update 21.08.13: prevent large penalty from incurring too high of negative reward
            if penalty_h > 1:
                reward_h /= penalty_h
            if penalty_r > 1:
                reward_r /= penalty_r

            reward_ = {'robot': reward_r, 'human': reward_h}
            done_ = {'robot': done, 'human': done, '__all__': done}
            info_ = {'robot': info, 'human': info}

            if self.latent_current_human is not None:
                reward_ = {'robot': reward_r, self.latent_current_human: reward_h}
                done_ = {'robot': done, self.latent_current_human: done, '__all__': done}
                info_ = {'robot': info, self.latent_current_human: info}

            if self.return_robot_only:
                return obs['robot'], reward_['robot'], done_['robot'], info_['robot']

            return obs, reward_, done_, info_

    def get_total_force(self):
        total_force_on_human = np.sum(self.robot.get_contact_points(self.human)[-1])
        tool_force = np.sum(self.tool.get_contact_points()[-1])
        tool_force_on_human = 0
        new_contact_points = 0
        for linkA, linkB, posA, posB, force in zip(*self.tool.get_contact_points(self.human)):
            total_force_on_human += force
            if linkA in [1]:
                tool_force_on_human += force
                # Only consider contact with human upperarm, forearm, hand
                if linkB < 0 or linkB > len(self.human.all_joint_indices):
                    continue

                indices_to_delete = []
                for i, (target_pos_world, target) in enumerate(zip(self.targets_pos_upperarm_world, self.targets_upperarm)):
                    if np.linalg.norm(posB - target_pos_world) < 0.025:
                        # The robot made contact with a point on the person's arm
                        new_contact_points += 1
                        self.task_success += 1
                        target.set_base_pos_orient(self.np_random.uniform(1000, 2000, size=3), [0, 0, 0, 1])
                        indices_to_delete.append(i)
                self.targets_pos_on_upperarm = [t for i, t in enumerate(self.targets_pos_on_upperarm) if i not in indices_to_delete]
                self.targets_upperarm = [t for i, t in enumerate(self.targets_upperarm) if i not in indices_to_delete]
                self.targets_pos_upperarm_world = [t for i, t in enumerate(self.targets_pos_upperarm_world) if i not in indices_to_delete]

                indices_to_delete = []
                for i, (target_pos_world, target) in enumerate(zip(self.targets_pos_forearm_world, self.targets_forearm)):
                    if np.linalg.norm(posB - target_pos_world) < 0.025:
                        # The robot made contact with a point on the person's arm
                        new_contact_points += 1
                        self.task_success += 1
                        target.set_base_pos_orient(self.np_random.uniform(1000, 2000, size=3), [0, 0, 0, 1])
                        indices_to_delete.append(i)
                self.targets_pos_on_forearm = [t for i, t in enumerate(self.targets_pos_on_forearm) if i not in indices_to_delete]
                self.targets_forearm = [t for i, t in enumerate(self.targets_forearm) if i not in indices_to_delete]
                self.targets_pos_forearm_world = [t for i, t in enumerate(self.targets_pos_forearm_world) if i not in indices_to_delete]

        return tool_force, tool_force_on_human, total_force_on_human, new_contact_points

    def _get_obs(self, agent=None):
        end_effector_pos, end_effector_orient = self.robot.get_pos_orient(self.robot.left_end_effector)
        end_effector_pos_real, end_effector_orient_real = self.robot.convert_to_realworld(end_effector_pos, end_effector_orient)
        robot_joint_angles = self.robot.get_joint_angles(self.robot.controllable_joint_indices)
        # Fix joint angles to be in [-pi, pi]
        robot_joint_angles = (np.array(robot_joint_angles) + np.pi) % (2*np.pi) - np.pi
        if self.robot.mobile:
            # Don't include joint angles for the wheels
            robot_joint_angles = robot_joint_angles[len(self.robot.wheel_joint_indices):]
        shoulder_pos = self.human.get_pos_orient(self.human.right_upperarm)[0]
        elbow_pos = self.human.get_pos_orient(self.human.right_forearm)[0]
        wrist_pos = self.human.get_pos_orient(self.human.right_hand)[0]
        shoulder_pos_real, _ = self.robot.convert_to_realworld(shoulder_pos)
        elbow_pos_real, _ = self.robot.convert_to_realworld(elbow_pos)
        wrist_pos_real, _ = self.robot.convert_to_realworld(wrist_pos)
        self.tool_force, self.tool_force_on_human, self.total_force_on_human, self.new_contact_points = self.get_total_force()
        robot_obs = np.concatenate([end_effector_pos_real, end_effector_orient_real, robot_joint_angles, shoulder_pos_real, elbow_pos_real, wrist_pos_real, [self.tool_force]]).ravel()
        if agent == 'robot':
            return robot_obs
        if self.human.controllable:
            human_joint_angles = self.human.get_joint_angles(self.human.controllable_joint_indices)
            end_effector_pos_human, end_effector_orient_human = self.human.convert_to_realworld(end_effector_pos, end_effector_orient)
            shoulder_pos_human, _ = self.human.convert_to_realworld(shoulder_pos)
            elbow_pos_human, _ = self.human.convert_to_realworld(elbow_pos)
            wrist_pos_human, _ = self.human.convert_to_realworld(wrist_pos)
            human_obs = np.concatenate([end_effector_pos_human, end_effector_orient_human, human_joint_angles, shoulder_pos_human, elbow_pos_human, wrist_pos_human, [self.total_force_on_human, self.tool_force_on_human]]).ravel()
            if agent == 'human':
                return human_obs
            # Co-optimization with both human and robot controllable
            return {'robot': robot_obs, 'human': human_obs}
        return robot_obs

    def reset(self):
        super(BedBathingEnv, self).reset()
        self.build_assistive_env('hospital_bed', fixed_human_base=False, human_impairment=self._impairment)
        self.furniture.set_on_ground()

        self.furniture.set_joint_angles([1], [np.pi/3])
        # self.furniture.set_joint_angles([1], [np.pi/4])
        # self.furniture.set_joint_angles([1], [0])
        self.furniture.set_whole_body_frictions(1, 1, 1)

        # Setup human in the air and let them settle into a resting pose on the bed
        # joints_positions = [(self.human.j_right_shoulder_x, 30), (self.human.j_left_shoulder_x, -30), (self.human.j_waist_x, 45)]
        joints_positions = [(self.human.j_right_shoulder_x, 30), (self.human.j_left_shoulder_x, -30), (self.human.j_waist_x, 60)]
        self.human.setup_joints(joints_positions, use_static_joints=True, reactive_force=None)
        # self.human.set_base_pos_orient([0, -0.05, 0.85], [-np.pi/2.0, 0, 0])
        self.human.set_base_pos_orient([0, -0.15, 0.85], [-np.pi/2, 0, 0])

        # Stiffen human chest joints to prevent the robot from collapsing in on itself when hitting the bed
        stiffness = self.human.get_joint_stiffness()
        for joint in [self.human.j_waist_x, self.human.j_waist_y, self.human.j_waist_z, self.human.j_chest_x, self.human.j_chest_y, self.human.j_chest_z, self.human.j_upper_chest_x, self.human.j_upper_chest_y, self.human.j_upper_chest_z]:
            self.human.set_joint_stiffness(joint, 1)

        p.setGravity(0, 0, -1, physicsClientId=self.id)

        # Add small variation in human joint positions
        motor_indices, motor_positions, motor_velocities, motor_torques = self.human.get_motor_joint_states()
        # self.human.set_joint_angles(motor_indices, motor_positions+self.np_random.uniform(-0.1, 0.1, size=len(motor_indices)))

        # Let the person settle on the bed
        for _ in range(100):
            p.stepSimulation(physicsClientId=self.id)
            # time.sleep(0.5)

        # Reset joint stiffness to default values
        # for joint, s in zip(self.human.all_joint_indices, stiffness):
        #     self.human.set_joint_stiffness(joint, s)

        # Stiffen uncontrolled joints and freeze the human base on the bed
        self.human.control(self.human.all_joint_indices, self.human.get_joint_angles(), 0.05, 100)
        self.human.set_mass(self.human.base, mass=0)
        self.human.set_base_velocity(linear_velocity=[0, 0, 0], angular_velocity=[0, 0, 0])

        shoulder_pos = self.human.get_pos_orient(self.human.right_upperarm)[0]
        elbow_pos = self.human.get_pos_orient(self.human.right_forearm)[0]
        wrist_pos = self.human.get_pos_orient(self.human.right_hand)[0]

        # Initialize the tool in the robot's gripper
        self.tool.init(self.robot, self.task, self.directory, self.id, self.np_random, right=False, mesh_scale=[1]*3)
        # target_ee_pos = np.array([-0.6, -0.1, 0.8]) + self.np_random.uniform(-0.05, 0.05, size=3)
        target_ee_pos = np.array([-0., -0.1, 0.8])
        target_ee_orient = self.get_quaternion(self.robot.toc_ee_orient_rpy[self.task])

        collision_objects = [self.human, self.furniture]
        if self.no_furniture_collision:
            collision_objects = [self.human]

        if not self._fix_base:
            base_position = self.init_robot_pose(target_ee_pos, target_ee_orient, [(target_ee_pos, target_ee_orient)], [(shoulder_pos, None), (elbow_pos, None), (wrist_pos, None)], arm='left', tools=[self.tool], collision_objects=collision_objects, wheelchair_enabled=False)
        else:
            base_position = self.init_robot_pose_fix(target_ee_pos, target_ee_orient, [(target_ee_pos, target_ee_orient)], [(shoulder_pos, None), (elbow_pos, None), (wrist_pos, None)], arm='left', tools=[self.tool], collision_objects=collision_objects, wheelchair_enabled=False)


        if self.robot.wheelchair_mounted:
            # Load a nightstand in the environment for mounted arms
            self.nightstand = Furniture()
            self.nightstand.init('nightstand', self.directory, self.id, self.np_random)
            nightstand_pos = np.array([-0.85, -0.35, 0])
            nightstand_pos[:2] += self.robot.toc_base_pos_offset[self.task][:2]
            self.nightstand.set_base_pos_orient(nightstand_pos, [0, 0, 0, 1])

        # Open gripper to hold the tool
        self.robot.set_gripper_open_position(self.robot.left_gripper_indices, self.robot.gripper_pos[self.task], set_instantly=True)
        if self._replay_pose_player is not None:
            self.set_human_joints(self._replay_pose_player.get_joint_data(0))


        self.generate_targets()

        p.setGravity(0, 0, -9.81, physicsClientId=self.id)
        if not self.robot.mobile:
            self.robot.set_gravity(0, 0, 0)
        self.human.set_gravity(0, 0, 0)
        self.tool.set_gravity(0, 0, 0)

        # Enable rendering
        p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, 1, physicsClientId=self.id)

        self.init_env_variables()
        obs = self._get_obs()

        # Robot-only environment, compute and cache human actions
        if self.latent_args is not None:
            # Reset current latent human every reset()
            self.latent_current_human = self.np_random.choice(self.latent_args["human_keys"])
            robot_obs = obs["robot"]
            human_obs = obs["human"]
            del obs["robot"]
            del obs["human"]
            # else:
            #     raise NotImplementedError
            obs[self.latent_current_human] = human_obs
            obs["robot"] = robot_obs

            if self.latent_args["load_pytorch"]:
                self._last_human_action, _ = self.human_policies[self.latent_current_human].compute_action(torch.tensor([human_obs]).float(), explore=True)
                self._last_human_obs = human_obs
                self._last_human_action = self._last_human_action.numpy()[0]
        if self._replay_pose_player is not None:
            human_obs = obs["human"]
            self._last_human_obs = human_obs
            self._last_human_action = self._replay_pose_player.get_next_action(self.iteration)

        if self.return_robot_only:
            return obs['robot']

        p.resetDebugVisualizerCamera(cameraDistance=1.10, cameraYaw=40, cameraPitch=-45, cameraTargetPosition=[-0.2, 0, 0.75], physicsClientId=self.id)
        # p.resetDebugVisualizerCamera(cameraDistance=1.10, cameraYaw=90, cameraPitch=-15, cameraTargetPosition=[0.2, -0.3, 0.35], physicsClientId=self.id)
        return obs

    def init_robot_pose_fix(self, target_ee_pos, target_ee_orient, start_pos_orient, target_pos_orients, arm='right', tools=[], collision_objects=[], wheelchair_enabled=True, right_side=True, max_iterations=3):
        base_position = None
        if self.robot.skip_pose_optimization:
            return base_position
        # Continually resample initial robot pose until we find one where the robot isn't colliding with the person
        # Randomize robot base pose
        if self.robot.mobile:
            pos = np.array(self.robot.toc_base_pos_offset[self.task])
            orient = np.array(self.robot.toc_ee_orient_rpy[self.task])
            if self.task == 'dressing':
                orient = orient[0]
            self.robot.set_base_pos_orient(pos, orient)
            # elif self.robot.wheelchair_mounted and wheelchair_enabled:
            #     # Use IK to find starting joint angles for mounted robots
            self.robot.randomize_init_joint_angles(self.task)
        elif self.robot.wheelchair_mounted and wheelchair_enabled:
            self.robot.ik_random_restarts(right=(arm == 'right'), target_pos=target_ee_pos, target_orient=target_ee_orient, max_iterations=1000, max_ik_random_restarts=1000, success_threshold=0.01, step_sim=False, check_env_collisions=False, randomize_limits=True, collision_objects=collision_objects)
        else:
            # Use TOC with JLWKI to find an optimal base position for the robot near the person
            rpos, rorn = 0.05, 10
            base_euler_orient=[0, 0, 0 if right_side else np.pi]
            random_pos = np.array([self.np_random.uniform(-rpos if right_side else 0, 0 if right_side else rpos), self.np_random.uniform(-rpos, rpos), 0])
            random_orientation = np.deg2rad(self.np_random.uniform(-rorn, rorn))

            base_position = np.array([-0.85, -0.4, 0]) + self.robot.toc_base_pos_offset[self.task] + random_pos
            base_orientation = self.robot.toc_base_orient_offset[self.task] + random_orientation

            self.robot.set_base_pos_orient(base_position, base_orientation)
            self.robot.randomize_init_joint_angles(self.task)

        dists_list = []
        for tool in tools:
            tool.reset_pos_orient()
            for obj in collision_objects:
                dists_list.append(tool.get_closest_points(obj, distance=0)[-1])
        for obj in collision_objects:
            dists_list.append(self.robot.get_closest_points(obj, distance=0)[-1])
        return base_position


    def generate_targets(self):
        self.target_indices_to_ignore = []
        self.upperarm, self.upperarm_length, self.upperarm_radius = self.human.right_upperarm, self.human.upperarm_length, self.human.upperarm_radius
        self.forearm, self.forearm_length, self.forearm_radius = self.human.right_forearm, self.human.forearm_length, self.human.forearm_radius
        self.targets_pos_on_upperarm = self.util.capsule_points(p1=np.array([0, 0, -self.human.pecs_offset]), p2=np.array([0, 0, -self.upperarm_length]), radius=self.upperarm_radius, distance_between_points=0.03, num_sections=7, points_per_section=10)
        self.targets_pos_on_forearm = self.util.capsule_points(p1=np.array([0, 0, 0]), p2=np.array([0, 0, -self.forearm_length]), radius=self.forearm_radius, distance_between_points=0.03, num_sections=7, points_per_section=6)

        self.targets_upperarm = self.create_spheres(radius=0.01, mass=0.0, batch_positions=[[0, 0, 0]]*len(self.targets_pos_on_upperarm), visual=True, collision=False, rgba=[0, 1, 1, 1])
        self.targets_forearm = self.create_spheres(radius=0.01, mass=0.0, batch_positions=[[0, 0, 0]]*len(self.targets_pos_on_forearm), visual=True, collision=False, rgba=[0, 1, 1, 1])
        self.total_target_count = len(self.targets_pos_on_upperarm) + len(self.targets_pos_on_forearm)
        self.update_targets()

    def update_targets(self):
        upperarm_pos, upperarm_orient = self.human.get_pos_orient(self.upperarm)
        self.targets_pos_upperarm_world = []
        for target_pos_on_arm, target in zip(self.targets_pos_on_upperarm, self.targets_upperarm):
            target_pos = np.array(p.multiplyTransforms(upperarm_pos, upperarm_orient, target_pos_on_arm, [0, 0, 0, 1], physicsClientId=self.id)[0])
            self.targets_pos_upperarm_world.append(target_pos)
            target.set_base_pos_orient(target_pos, [0, 0, 0, 1])

        forearm_pos, forearm_orient = self.human.get_pos_orient(self.forearm)
        self.targets_pos_forearm_world = []
        for target_pos_on_arm, target in zip(self.targets_pos_on_forearm, self.targets_forearm):
            target_pos = np.array(p.multiplyTransforms(forearm_pos, forearm_orient, target_pos_on_arm, [0, 0, 0, 1], physicsClientId=self.id)[0])
            self.targets_pos_forearm_world.append(target_pos)
            target.set_base_pos_orient(target_pos, [0, 0, 0, 1])

