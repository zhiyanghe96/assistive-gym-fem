from .bed_bathing import BedBathingEnv
from .agents import pr2, baxter, sawyer, jaco, stretch, panda, human
from .agents.pr2 import PR2
from .agents.baxter import Baxter
from .agents.sawyer import Sawyer
from .agents.jaco import Jaco
from .agents.stretch import Stretch
from .agents.panda import Panda
from .agents.human import Human
from ray.rllib.env.multi_agent_env import MultiAgentEnv
from ray.tune.registry import register_env
import json
import numpy as np

robot_arm = 'left'
human_controllable_joint_indices = human.bath_joints + human.right_arm_joints

class BedBathingPR2Env(BedBathingEnv):
    def __init__(self):
        super(BedBathingPR2Env, self).__init__(robot=PR2(robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingBaxterEnv(BedBathingEnv):
    def __init__(self):
        super(BedBathingBaxterEnv, self).__init__(robot=Baxter(robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingSawyerEnv(BedBathingEnv):
    def __init__(self):
        super(BedBathingSawyerEnv, self).__init__(robot=Sawyer(robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingJacoEnv(BedBathingEnv):
    def __init__(self):
        super(BedBathingJacoEnv, self).__init__(robot=Jaco(robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingStretchEnv(BedBathingEnv):
    def __init__(self):
        super(BedBathingStretchEnv, self).__init__(robot=Stretch('wheel_'+robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingPandaEnv(BedBathingEnv):
    def __init__(self):
        super(BedBathingPandaEnv, self).__init__(robot=Panda(robot_arm), human=Human(human_controllable_joint_indices, controllable=False))

class BedBathingPR2HumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingPR2HumanEnv, self).__init__(robot=PR2(robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemPR2Human-v1', lambda config: BedBathingPR2HumanEnv())


class BedBathingPR2HumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingPR2HumanPCAEnv, self).__init__(robot=PR2(robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)


class BedBathingBaxterHumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingBaxterHumanEnv, self).__init__(robot=Baxter(robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemBaxterHuman-v1', lambda config: BedBathingBaxterHumanEnv())


class BedBathingBaxterHumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingBaxterHumanPCAEnv, self).__init__(robot=Baxter(robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)


class BedBathingSawyerHumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingSawyerHumanEnv, self).__init__(robot=Sawyer(robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemSawyerHuman-v1', lambda config: BedBathingSawyerHumanEnv())


class BedBathingSawyerHumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingSawyerHumanPCAEnv, self).__init__(robot=Sawyer(robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)



class BedBathingJacoHumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingJacoHumanEnv, self).__init__(robot=Jaco(robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemJacoHuman-v1', lambda config: BedBathingJacoHumanEnv())


class BedBathingJacoHumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingJacoHumanPCAEnv, self).__init__(robot=Jaco(robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)


class BedBathingStretchHumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingStretchHumanEnv, self).__init__(robot=Stretch('wheel_'+robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemStretchHuman-v1', lambda config: BedBathingStretchHumanEnv())


class BedBathingStretchHumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingStretchHumanPCAEnv, self).__init__(robot=Stretch('wheel_'+robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)



class BedBathingPandaHumanEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', *args, **kwargs):
        super(BedBathingPandaHumanEnv, self).__init__(robot=Panda(robot_arm), human=Human(human_controllable_joint_indices, controllable=True), collab_version=collab_version, impairment=impairment, *args, **kwargs)
register_env('assistive_gym:BedBathingFemPandaHuman-v1', lambda config: BedBathingPandaHumanEnv())


class BedBathingPandaHumanPCAEnv(BedBathingEnv, MultiAgentEnv):
    def __init__(self, impairment="random", collab_version='v4', motion_pca_file=None, *args, **kwargs):
        motion_pca_file = motion_pca_file
        with open(motion_pca_file, "r") as f:
            pca_data = json.load(f)
            pca_controllable_joints = pca_data["keys"]
        super(BedBathingPandaHumanPCAEnv, self).__init__(robot=Panda(robot_arm), human=Human(pca_controllable_joints, controllable=True), collab_version=collab_version, impairment=impairment, motion_pca_file=motion_pca_file, *args, **kwargs)






""" 2021.04.27
- Train with human agents
"""

robot_envs = {
    'PR2': BedBathingPR2HumanEnv,
    'Jaco': BedBathingJacoHumanEnv,
    'Baxter': BedBathingBaxterHumanEnv,
    'Sawyer': BedBathingSawyerHumanEnv,
    'Stretch': BedBathingStretchHumanEnv,
    'Panda': BedBathingPandaHumanEnv
}


for rname, renv in robot_envs.items():

    # Personalization
    register_env(f'assistive_gym:BedBathingFem{rname}Personalized-noimpair-v1', lambda config: renv(impairment="none"))

    # Same env as Personalization
    register_env(f'assistive_gym:BedBathingFem{rname}Personalized-v1', lambda config: renv(impairment="none"))

    for collab_v in ["v0217_r80", "v0217_r40", "v0217_r20", "v0217_r8", "v0217_r4", "v0217_r1", "v0217_r05", "v0217_h100", "v0217_h60", "v0217_h30", "v0217_h10", "v0217_h3", "v0217_h1", "v0217_h05", "v0217_h02", "v0217_0"]:

        register_env(
            f'assistive_gym:BedBathingFem{rname}Human-{collab_v}-v1',
            lambda config: renv(
                collab_version=collab_v,
                impairment="none"
            )
        )

        register_env(
            f'assistive_gym:BedBathingFem{rname}HumanFix-{collab_v}-v1',
            lambda config: renv(
                collab_version=collab_v,
                impairment="none",
                fix_base=True
            )
        )


        register_env(
            f'assistive_gym:BedBathingFem{rname}Human-{collab_v}-v2',
            lambda config: renv(
                collab_version=collab_v,
                impairment="none",
                fix_base=True,
                add_offset=True
            )
        )
