from .bed_bathing_envs import BedBathingPR2HumanEnv, BedBathingJacoHumanEnv, BedBathingSawyerHumanEnv, BedBathingBaxterHumanEnv, BedBathingPandaHumanEnv, BedBathingStretchHumanEnv
from .bed_bathing_envs import BedBathingPR2HumanPCAEnv, BedBathingJacoHumanPCAEnv, BedBathingSawyerHumanPCAEnv, BedBathingBaxterHumanPCAEnv, BedBathingPandaHumanPCAEnv, BedBathingStretchHumanPCAEnv

_v220131_hpath = "human_models/220131/ppo/BedBathingRRRHumanPCAv1Env-v0217_xxx-v3-human_scratch-robot_scratch-s1/checkpoint_final/checkpoint-final"


v220131_humans = {}
for robot in ["Jaco", "Baxter", "Sawyer", "PR2", "Panda", "Stretch"]:
    for effort in ["0", "h1", "h3", "h10", "r4"]:
        v220131_humans[f"{robot}-Human-{effort}"] = _v220131_hpath.replace("RRR", robot).replace("xxx", effort)

v220131_metadata = {
    "human_data": v220131_humans,
    "envs": [
        {
            "base_env": BedBathingJacoHumanPCAEnv,
            "base_name": "BedBathingJacoHumanPCAEnv",
            "humans": {
                "BedBathingJacoPersonalize220131Ver0_001-v1": ["Jaco-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_002-v1": ["Jaco-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_003-v1": ["Jaco-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_004-v1": ["Jaco-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_005-v1": ["Jaco-Human-r4"],

                "BedBathingJacoPersonalize220131Ver0_011-v1": ["Baxter-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_012-v1": ["Baxter-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_013-v1": ["Baxter-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_014-v1": ["Baxter-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_015-v1": ["Baxter-Human-r4"],

                "BedBathingJacoPersonalize220131Ver0_021-v1": ["Sawyer-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_022-v1": ["Sawyer-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_023-v1": ["Sawyer-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_024-v1": ["Sawyer-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_025-v1": ["Sawyer-Human-r4"],

                "BedBathingJacoPersonalize220131Ver0_031-v1": ["PR2-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_032-v1": ["PR2-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_033-v1": ["PR2-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_034-v1": ["PR2-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_035-v1": ["PR2-Human-r4"],

                "BedBathingJacoPersonalize220131Ver0_041-v1": ["Panda-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_042-v1": ["Panda-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_043-v1": ["Panda-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_044-v1": ["Panda-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_045-v1": ["Panda-Human-r4"],

                "BedBathingJacoPersonalize220131Ver0_051-v1": ["Stretch-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_052-v1": ["Stretch-Human-h1"],
                "BedBathingJacoPersonalize220131Ver0_053-v1": ["Stretch-Human-h3"],
                "BedBathingJacoPersonalize220131Ver0_054-v1": ["Stretch-Human-h10"],
                "BedBathingJacoPersonalize220131Ver0_055-v1": ["Stretch-Human-r4"],


                "BedBathingJacoPersonalize220131Ver0_061-v1": ["Baxter-Human-0", "Sawyer-Human-0", "PR2-Human-0", "Stretch-Human-0"],
                "BedBathingJacoPersonalize220131Ver0_062-v1": ["Baxter-Human-0", "Sawyer-Human-0", "PR2-Human-0", "Stretch-Human-0", "Panda-Human-0", "Jaco-Human-0"],


            }
        }
    ]
}
