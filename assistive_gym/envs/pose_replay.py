import json
import numpy as np
from .env import ACTION_MULTIPLIER


class PosePlayer(object):
    def __init__(self, env, replay_pose_file, task, max_len=200):
        self.env = env
        self.pose_file = replay_pose_file
        self.replay_pose_file = replay_pose_file
        self.replay_pose_data = None
        if replay_pose_file:
            with open(replay_pose_file, "r") as f:
                self.replay_pose_data = json.load(f)
        self.max_len = max_len

        self.joint_mapping = {}
        if task == "bed_bathing":
            self.joint_mapping = {
                # 0: 0, # waist
                # 1: 1, # waist
                # 2: 2, # waist
                # 3: 3, # chest
                # 4: 4, # chest
                # 5: 5, # chest
                0: 3, # waist
                1: 4, # waist
                2: 5, # waist
                6: 6, # upper chest
                7: 12, # left shoulder
                8: 13, # left shoulder
                9: 14, # left shoulder
                10: 15, # elbow
                11: 16, # hand
                12: 17, # hand
                13: 18, # hand
                14: -1,
                15: -1,
                16: -1,
                17: 22, # right arm
                18: 23, # right arm
                19: 24, # right arm
                20: 25, # right arm
                21: 26, # right arm
                22: 27, # right arm
                23: 28,  # right arm
                24: -1,  # right arm
                25: 30, # head joint
                26: 31, # head joint
                27: 32, # head joint
            }

    def get_joint_angles(self, t):
        curr_angles = self._compute_joint_angles(0)
        return pose

    def get_joint_data(self, t):
        pose_data = self.replay_pose_data[t]
        converted_data = {}
        for j, data in pose_data.items():
            if int(j) in self.joint_mapping:
                converted_data[self.joint_mapping[int(j)]] = data
        return converted_data

    def get_next_action(self, t):
        next_angles = self._compute_joint_angles(min(t + 1, self.max_len - 1))
        curr_angles = self._compute_joint_angles(min(t, self.max_len - 1))
        human_indices = self.env.human.controllable_joint_indices
        return (next_angles - curr_angles) / ACTION_MULTIPLIER

    def _compute_joint_angles(self, t):
        human_joints = self.env.human.controllable_joint_indices
        angles = np.zeros(len(human_joints))
        try:
            pose = self.replay_pose_data[t]
        except:
            print("Bad timestep", t)
        for json_idx, joint_idx in self.joint_mapping.items():
            if str(json_idx) in pose.keys() and joint_idx in human_joints:
                angles[human_joints.index(joint_idx)] = pose[str(json_idx)][0]
        return angles
