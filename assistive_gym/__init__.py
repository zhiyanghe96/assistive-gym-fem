from gym.envs.registration import register
import os

tasks = ['ScratchItch', 'BedBathing', 'Feeding', 'Drinking', 'Dressing', 'ArmManipulation']
robots = ['PR2', 'Jaco', 'Baxter', 'Sawyer', 'Stretch', 'Panda']

for task in tasks:
    for robot in robots:
        register(
            id='%s%s-v1' % (task, robot),
            entry_point='assistive_gym.envs:%s%sEnv' % (task, robot),
            max_episode_steps=200,
        )

        register(
            id='%s%sHuman-v1' % (task, robot),
            entry_point='assistive_gym.envs:%s%sHumanEnv' % (task, robot),
            max_episode_steps=200,
        )

for task in ['ScratchItch', 'Feeding']:
    for robot in robots:
        register(
            id='%s%sMesh-v1' % (task, robot),
            entry_point='assistive_gym.envs:%s%sMeshEnv' % (task, robot),
            max_episode_steps=200,
        )

register(
    id='DressingPR2Mesh-v1',
    entry_point='assistive_gym.envs:DressingPR2MeshEnv',
    max_episode_steps=200,
)
register(
    id='DressingPR2IKMesh-v1',
    entry_point='assistive_gym.envs:DressingPR2IKMeshEnv',
    max_episode_steps=200,
)
register(
    id='DressingPR2IK-v1',
    entry_point='assistive_gym.envs:DressingPR2IKEnv',
    max_episode_steps=200,
)

register(
    id='BedPosePR2-v1',
    entry_point='assistive_gym.envs:BedPosePR2Env',
    max_episode_steps=200,
)
register(
    id='BedPosePR2Mesh-v1',
    entry_point='assistive_gym.envs:BedPosePR2MeshEnv',
    max_episode_steps=200,
)
register(
    id='BedPoseStretch-v1',
    entry_point='assistive_gym.envs:BedPoseStretchEnv',
    max_episode_steps=200,
)
register(
    id='BedPoseStretchMesh-v1',
    entry_point='assistive_gym.envs:BedPoseStretchMeshEnv',
    max_episode_steps=200,
)


register(
    id='HumanTesting-v1',
    entry_point='assistive_gym.envs:HumanTestingEnv',
    max_episode_steps=200,
)

register(
    id='SMPLXTesting-v1',
    entry_point='assistive_gym.envs:SMPLXTestingEnv',
    max_episode_steps=200,
)

register(
    id='ViewClothVertices-v1',
    entry_point='assistive_gym.envs:ViewClothVerticesEnv',
    max_episode_steps=1000000,
)


from assistive_gym.envs.bed_bathing_latent import v220131_metadata

"""2021.11.23
(1) Did not enforce fix base
"""
for env_item in v220131_metadata["envs"]:
    base_name = env_item["base_name"]

    for env_name, human_keys in env_item["humans"].items():
        agent = "robot"
        latent_args = {
            "human_keys": human_keys,
            "agent": agent,
            "load_pytorch": True,
            "human_paths": "v220131_humans"
        }
        register(
            id=env_name,
            entry_point=f'assistive_gym.envs.bed_bathing_envs:{base_name}',
            kwargs={
                "impairment": "none",
                "latent_args": latent_args,
                "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v1.json'),
                "collab_version": "v0217_0",
                "fix_base": True,
                "offset_version": "v2",
                "init_success_threshold": 2.,
                "return_robot_only": True
            }
        )



""" 2022.01.31 Pose Env
"""
for robot in robots:
    # Load from pose_v1 files
    pose_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "envs", "assets", "poses")
    for idx, pose_file in enumerate(sorted(os.listdir(pose_folder))):
        register(
            id=f"BedBathing{robot}HumanPCAv1Replayv1{idx:02d}Env-v0217_0-v3",
            entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
            kwargs={
                "impairment": "none",
                "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v1.json'),
                "collab_version": "v0217_0",
                "no_furniture_collision": True,
                "fix_base": True,
                "offset_version": "v2",
                "init_success_threshold": 2.,
                "replay_pose_file": os.path.join(pose_folder, pose_file),
                "return_robot_only": True
            }
        )



""" 2021.11.20 PCA Env
1) Multiply by max(pca_fit) / 10
"""
for robot in robots:
    # Vanilla version without changing reward penalty
    for pca_version in range(1, 3):
        register(
            id=f"BedBathing{robot}HumanPCAv{pca_version}Env-v1",
            entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
            kwargs={
                "impairment": "none",
                "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v{pca_version}.json')
            }
        )

    for collab_v in ["v0217_r80", "v0217_r40", "v0217_r20", "v0217_r8", "v0217_r4", "v0217_r1", "v0217_r05", "v0217_h100", "v0217_h60", "v0217_h30", "v0217_h10", "v0217_h3", "v0217_h1", "v0217_h05", "v0217_h02", "v0217_0"]:
        for pca_version in range(1, 3):
            register(
                id=f"BedBathing{robot}HumanPCAv{pca_version}Env-{collab_v}-v1",
                entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
                kwargs={
                    "impairment": "none",
                    "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v{pca_version}.json'),
                    "collab_version": collab_v
                }
            )

            register(
                id=f"BedBathing{robot}HumanPCAv{pca_version}Env-{collab_v}-v2",
                entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
                kwargs={
                    "impairment": "none",
                    "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v{pca_version}.json'),
                    "collab_version": collab_v,
                    "fix_base": True,
                    "offset_version": "v2",
                    "init_success_threshold": 2.
                }
            )

            register(
                id=f"BedBathing{robot}HumanPCAv{pca_version}Env-{collab_v}-v3",
                entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
                kwargs={
                    "impairment": "none",
                    "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v{pca_version}.json'),
                    "collab_version": collab_v,
                    "fix_base": True,
                    "offset_version": "v2",
                    "init_success_threshold": 2.
                }
            )

            register(
                id=f"BedBathing{robot}HumanPCAv{pca_version}Env-{collab_v}-v4",
                entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{robot}HumanPCAEnv',
                kwargs={
                    "impairment": "none",
                    "motion_pca_file": os.path.join(os.path.dirname(os.path.realpath(__file__)), 'envs', 'assets', f'pca_poses_v{pca_version}.json'),
                    "collab_version": collab_v,
                    "no_furniture_collision": True,
                    "offset_version": "v2",
                    "init_success_threshold": 2.
                }
            )


""" 2021.04.27
- Personalize with human agents
"""
for rname in robots:
    # Personalization
    register(
        id=f'BedBathingFem{rname}Personalized-v1',
        entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{rname}HumanEnv',
        kwargs={
            "impairment": "none",
        }
    )
    # Personalization
    register(
        id=f'BedBathingFem{rname}Personalized-noimpair-v1',
        entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{rname}HumanEnv',
        kwargs={
            "impairment": "none",
        }
    )
    for collab_v in ["v0217_r80", "v0217_r40", "v0217_r20", "v0217_r8", "v0217_r4", "v0217_r1", "v0217_r05", "v0217_h100", "v0217_h60", "v0217_h30", "v0217_h10", "v0217_h3", "v0217_h1", "v0217_h05", "v0217_h02", "v0217_0"]:
        # print(f'BedBathingFem{rname}Human-{collab_v}-v1')
        register(
            id=f'BedBathingFem{rname}Human-{collab_v}-v1',
            entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{rname}HumanEnv',
            kwargs={
                "impairment": "none",
                "collab_version": collab_v
            }
        )

        register(
            id=f'BedBathingFem{rname}HumanFix-{collab_v}-v1',
            entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{rname}HumanEnv',
            kwargs={
                "impairment": "none",
                "collab_version": collab_v,
                "fix_base": True
            }
        )

        register(
            id=f'BedBathingFem{rname}Human-{collab_v}-v2',
            entry_point=f'assistive_gym.envs.bed_bathing_envs:BedBathing{rname}HumanEnv',
            kwargs={
                "impairment": "none",
                "collab_version": collab_v,
                "fix_base": True,
            }
        )

